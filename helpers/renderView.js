module.exports = (req, res, view, locals = {}, error = '') =>
	res.status(200).render('application', {
		locals: {
			user: req.user,
			csrfToken: req.csrfToken(),
			errors: error,
			...locals
		},
		partials: {
			yield: 'views' + view + '.html'
		}
	});
