const jwt = require('jsonwebtoken');

module.exports = {
	generateToken: user =>
		jwt.sign(
			{
				id: user.id,
				email: user.email,
				username: user.username
			},
			process.env.SECRET,
			{ expiresIn: '7d' }
		),
	validateToken: token => jwt.verify(token, process.env.SECRET)
};
