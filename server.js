const express = require('express');
const cluster = require('express-cluster');
const compression = require('compression');
const es6Renderer = require('express-es6-template-engine');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const middleware = require('./middleware/auth.js');
const methodOverride = require('method-override');

if (process.env.NODE_ENV !== 'production') {
	require('./env.js');
}

let app;

const initializeExpressServer = (worker = { id: 1 }) => {
	const app = express();
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(methodOverride('_method'));
	app.use(cookieParser());
	app.use(compression());

	// Middleware
	app.use(middleware.checkCSRF);
	app.use(middleware.checkCookie);

	// Views and Assets
	app.engine('html', es6Renderer);
	app.set('views', 'views');
	app.set('view engine', 'html');
	app.use(express.static(__dirname + '/assets'));

	// Routes
	app.use(require('./controllers'));

	const PORT = process.env.PORT || 3000;
	app.listen(PORT, error => {
		if (error) return console.error('Server Express Error:', error);
		console.log(`-- Server ${worker.id} listening on: ${PORT} --`);
	});
};

if (__DEVELOPMENT__) {
	initializeExpressServer();
} else {
	cluster(worker => initializeExpressServer(worker));
}
