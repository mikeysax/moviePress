const auth = require('../helpers/auth.js');

module.exports = {
	checkCookie: (req, res, next) => {
		const movieJWT = req.cookies.movie_press_token;
		if (!movieJWT) {
			req.user = null;
			next();
		}

		const tokenData = auth.validateToken(movieJWT);
		if (!tokenData) {
			res.clearCookie('movie_press_token');
			res.redirect('/');
			req.user = null;
			next();
		}

		req.user = {
			id: tokenData.id,
			username: tokenData.username,
			email: tokenData.email
		};
		next();
	},
	checkCSRF: (err, req, res, next) => {
		if (err.code !== 'EBADCSRFTOKEN') return next(err);
		// handle CSRF token errors here
		res.status(403);
		res.send('Form Tampered With!');
	}
};
