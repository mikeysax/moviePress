'use strict';
var bcrypt = require('bcryptjs');

module.exports = function(sequelize, DataTypes) {
	const User = sequelize.define(
		'User',
		{
			username: {
				type: DataTypes.STRING,
				unique: true,
				validate: {
					len: [4, 30],
					is: /^[a-z0-9]+$/i,
					notEmpty: true
				}
			},
			email: {
				type: DataTypes.STRING,
				unique: true,
				validate: {
					isEmail: true,
					notEmpty: true
				}
			},
			password: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true,
					len: [8, 50]
				}
			}
		},
		{}
	);

	User.beforeCreate(user => {
		let salt = bcrypt.genSaltSync(10);
		let hash = bcrypt.hashSync(user.password, salt);
		user.password = hash;
	});

	User.associate = function(models) {
		User.hasMany(models.Comment);
	};

	return User;
};
