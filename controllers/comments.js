const models = require('../models');
const form = require('../helpers/form');
const renderView = require('../helpers/renderView');

exports.new = (req, res, next) =>
	renderView(req, res, '/comments/new', { movieId: req.params.movieId });

exports.create = async (req, res, next) => {
	if (!req.user) throw new Error('Unauthorized');
	try {
		await models.Comment.create({
			body: req.body.body,
			rating: parseInt(req.body.rating),
			UserId: req.user.id,
			MovieId: req.params.movieId
		});
		return res.redirect(302, `/movies/${req.params.movieId}`);
	} catch ({ errors }) {
		const formErrors =
			typeof errors !== 'undefined' ? form.formErrors(errors) : '';
		return renderView(
			req,
			res,
			'/comments/new',
			{ movieId: req.params.movieId },
			formErrors
		);
	}
};

exports.edit = async (req, res, next) => {
	try {
		const comment = await models.Comment.findById(req.params.id);
		return renderView(req, res, '/comments/edit', {
			comment,
			movieId: comment.MovieId
		});
	} catch (error) {
		return res.redirect(302, `/movies/${req.params.movieId}`);
	}
};

exports.update = async (req, res, next) => {
	let comment;
	try {
		comment = await models.Comment.findById(req.params.id);
		if (comment.UserId !== req.user.id) throw new Error('Unauthorized');

		await comment.update({
			body: req.body.body,
			rating: parseInt(req.body.rating)
		});

		return res.redirect(302, `/movies/${req.params.movieId}`);
	} catch ({ errors }) {
		const commentErrors =
			typeof errors !== 'undefined' ? form.formErrors(errors) : '';
		return renderView(
			req,
			res,
			'/comments/edit',
			{ movieId: comment.MovieId, comment },
			commentErrors
		);
	}
};

exports.delete = async (req, res, next) => {
	try {
		const comment = await models.Comment.findById(req.params.id);
		if (comment.UserId !== req.user.id) throw new Error('Unauthorized');
		await comment.destroy();
		await res.redirect(`/movies/${comment.MovieId}`);
	} catch (error) {
		return res.redirect(302, `/movies/${req.params.movieId}`);
	}
};
