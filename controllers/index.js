const express = require('express');
const router = express.Router();

// Middleware
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });

// Controllers
const users = require('./users');
const movies = require('./movies');
const comments = require('./comments');

router
	// Movie Routes
	.get('/', movies.indexView)
	.get('/movies/:id', csrfProtection, movies.showView)
	// User Routes
	.get('/sign-up', csrfProtection, users.signupView)
	.post('/sign-up', csrfProtection, users.signup)
	.get('/sign-out', users.signout)
	.get('/sign-in', csrfProtection, users.signinView)
	.post('/sign-in', csrfProtection, users.signin)
	// Comment Routes
	.get('/comments', csrfProtection, comments.new)
	.post('/comments', csrfProtection, comments.create)
	.get('/comments/:id', csrfProtection, comments.edit)
	.put('/comments/:id', csrfProtection, comments.update)
	.delete('/comments/:id/delete', csrfProtection, comments.delete);

module.exports = router;
