const axios = require('axios');
const models = require('../models');
const renderView = require('../helpers/renderView');

const popularMoviesURL = `http://api.themoviedb.org/3/movie/popular?api_key=${
	process.env.MOVIE_API_KEY
}&language=en-US&page=1`;
const movieURL = `http://api.themoviedb.org/3/movie/${req.params.id}?api_key=${
	process.env.MOVIE_API_KEY
}&language=en-US`;

exports.indexView = async (req, res, next) => {
	try {
		const { data } = await axios.get(popularMoviesURL);
		return renderView(req, res, '/movies/index', {
			movies: data
		});
	} catch (error) {
		return res.status(404);
	}
};

exports.showView = async (req, res, next) => {
	try {
		const { data: movie } = await axios.get(movieURL);
		const comments = await models.Comment.findAll({
			where: { MovieId: req.params.id },
			include: [{ model: models.User, attributes: ['id', 'username'] }],
			order: '"createdAt" DESC'
		});
		return renderView(req, res, 'views/movies/show.html', {
			movie,
			comments
		});
	} catch (err) {
		return res.redirect(302, '/');
	}
};
