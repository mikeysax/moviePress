const models = require('../models');
const auth = require('../helpers/auth.js');
const bcrypt = require('bcryptjs');
const form = require('../helpers/form.js');
const renderView = require('../helpers/renderView');

exports.signupView = (req, res, next) =>
	renderView(req, res, '/users/sign-up.html');

exports.signup = async (req, res, next) => {
	try {
		const user = await models.User.create(req.body, {
			fields: ['username', 'email', 'password'],
			individualHooks: true
		});
		const token = auth.generateToken(user);
		res.cookie('movie_press_token', token, {
			httpOnly: true,
			maxAge: 86400000
		});
		return res.redirect(302, '/');
	} catch ({ errors }) {
		return renderView(
			req,
			res,
			'/users/sign-up',
			{},
			typeof errors !== 'undefined' ? form.formErrors(errors) : ''
		);
	}
};

exports.signout = (req, res, next) =>
	res.clearCookie('movie_press_token').redirect(302, '/');

exports.signinView = (req, res, next) => renderView(req, res, '/users/sign-in');

exports.signin = async (req, res, next) => {
	try {
		const errorMessage = 'Email or Password is incorrect.';

		const user = await models.User.findOne({
			where: { email: req.body.email }
		});
		if (!user) throw new Error(errorMessage);

		const match = await bcrypt.compare(req.body.password, user.password);
		if (!match) throw new Error(errorMessage);

		const token = auth.generateToken(user);

		return res
			.cookie('movie_press_token', token, {
				httpOnly: true,
				maxAge: 86400000
			})
			.redirect(302, '/');
	} catch ({ message }) {
		return renderView(req, res, '/users/sign-in', {}, message);
	}
};
