var fs = require('fs');
var babelrc = fs.readFileSync('./.babelrc');
var config;

try {
	config = JSON.parse(babelrc);
} catch (error) {
	console.error('ERROR: Error parsing your .babelrc.');
	console.error(error);
}

require('babel-core/register')(config);
//
//
var path = require('path');
var rootDir = path.resolve(__dirname, '');

global.__DEVELOPMENT__ = process.env.NODE_ENV === 'development';

// For .env file
// if (__DEVELOPMENT__) {
// 	require('./env.js');
// }

require('./server');
